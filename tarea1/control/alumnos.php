<?php
// Conexion a BD
// consulta SQL
class alumnos {

    public $id;
    public $nombre;
    public $sexo;

    public $alumno;

    public function __construct($id, $nombre, $sexo,$alumno) {
        $this->id     = $id;
        $this->nombre = $nombre;
        $this->sexo   = $sexo;  
        $this->alumno   = $alumno;     
    }

    public static function consultar() {
        include ('conexion.php');
        $consulta = "select * from alumnos";
        echo ('<br>');
        // echo ($consulta);
        $resultado = mysqli_query($mysqli, $consulta);
        if (!$resultado) {
            echo 'No pudo Realizar la consulta a la base de datos';
            exit;
        }
        $listaAlumnos = [];
        while ($alumno = mysqli_fetch_array($resultado)) {
            $listaAlumnos[] = new alumnos($alumno['id'],$alumno['alumno'], $alumno['nombre'], $alumno['sexo']);
        }
        return $listaAlumnos;
    }

}

?>
