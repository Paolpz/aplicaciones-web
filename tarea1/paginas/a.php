   
   <div class="card-panel  deep-orange">
        <span class="white-text text-darken-2" 3>De mis personajes favoritos!!!! Anime y Videojuegos</span>
      </div>
    
    <div id="carrusel">
        <a href="#" class="left-arrow"><img src="./imagenes/izquierda.png" /></a>
        <a href="#" class="right-arrow"><img src="./imagenes/derecha.png" /></a>
        <div class="carrusel">
            <div class="product" id="product_0">
                <img src="./imagenes/Daidouji.jpg" width="195" height="100" />
                <h5>Daidouji</h5>
            </div>
            <div class="product" id="product_1">
                <img src="./imagenes/baby-rosalina.jpg" width="195" height="100" />
                <h5>Baby Rosalina 2</h5>
            </div>
            <div class="product" id="product_2">
                <img src="./imagenes/pokemon.png" width="195" height="100" />
                <p><h5> Grookey</h5></p>
            </div>
            <div class="product" id="product_3">
                <img src="./imagenes/setsuna.jpg" width="195" height="100" />
                <h5>Setsuna</h5>
            </div>
            <div class="product" id="product_4">
                <img src="./imagenes/tae.jpg" width="195" height="100" />
                <h5>Yamada Tae</h5>
            </div>
            <div class="product" id="product_5">
                <img src="./imagenes/lucina.jpg" width="195" height="100" />
                <h5>Lucina</h5>
            </div>
        </div>
    </div>

<br /><br />
  <!--parrafos-->
      <h5 class="white-text">Daidouji </h5>
      <p>
          Daidouji (大道寺) es un personaje jugable de la serie de videojuegos Senran Kagura. Ella hace su debut en Senran Kagura: Portrait of Girls.
          Es una estudiante de tercer año de la Academia Hanzo. Es una estudiante mayor enigmática y legendaria, que emana un aura peligrosa.      
          A pesar de su edad y registro de asistencia escasa, ella sigue reinando sobre la Academia. Ella posee una increíble velocidad y un poder abrumador, y en contraste con su entrenamiento ninja, nunca se molesta en las tácticas de sigilo
          Su estilo de lucha hace temblar la tierra y el cielo, centrada en atacar a sus enemigos de frente con su enorme fuerza.
      </p>
      <br />
      <hr />
       <h5 class="white-text">Bebé Rosalina </h5>
      <p>
          Bebé Estela o Bebé Rosalina (en inglés: Baby Rosalina) es un personaje que apareció por primera vez en el juego Mario Kart 8, para la consola de sobremesa Wii U. Aunque apareció por primera vez en este juego Bebé Estela había sido creada y usada muchas veces por los fans en 
          páginas web fanon. Bebé Estela es la homóloga del pasado de Estela. Su color es el turquesa.
      </p>
      <br />
      <hr />
       <h5 class="white-text">Grookey </h5>
      <p>
          Grookey es un Pokémon de tipo planta introducido en la octava generación. Es uno de los Pokémon iniciales de Pokémon Espada y Pokémon Escudo
          Grookey es un mono verde con orejas y cola marrón, manos y hocico de color naranja, y una hoja con una ramita en su cabeza. La baqueta que lleva Grookey no era más que una ramita recogida del bosque en el que habita su manada,
          pero, tras estar expuesta a la energía que emana de su cuerpo, adquirió poderes especiales. El pelaje verde de Grookey sintetiza energía a partir de la luz solar. Las flores y hojas marchitas recuperan su color cuando Grookey golpea con su baqueta cerca de ellas.
        </p>
      <br />
      <hr />
       <h5 class="white-text"> Setsuna  </h5>
      <p>
          Setsuna (せつな) es una de las tres protagonistas de la serie de anime Hanyō no Yashahime.
          Es la menor de las gemelas hanyo que nacieron de la relación de Sesshomaru con su esposa[3] humana Rin[4]. Luego de separarse de su hermana sus sueños fueron consumidos por una Mariposa de los sueños, por lo que no es capaz de dormir y mucho menos recordar algo de su pasado.
          Mi nombre es Setsuna, y no tengo nada mas que decirte.. Por esa razón, este es tu fin” — Setsuna en el Trailer.
      </p>
      <br />
      <hr />
      <h5 class="white-text"> Yamada Tae </h5>
      <p>
          Yamada Tae (山田 た え) es una chica misteriosa con un pasado desconocido. Ella es la única zombie que no ha despertado. Kotaro no da ninguna explicación a quien es y cuando Sakura le pregunto el solo respondió "Quien dice que debe haber una leyenda para todo". También es miembro del grupo idol Franchouchou.
      La zombie de un largo pelo negro. La única zombie de los siete que aún no está completamente consciente. Como tal, ella es la más zombie del grupo, siempre tratando de morder algo. Aunque Tae solo habla en gemidos, parece ser capaz de entender lo que el resto de los miembros están
      diciendo hasta cierto punto. Debido a que ella es impulsada puramente por instinto, Tae siempre termina mordisqueando a Junko cuando está cerca.    
      </p>
      <br />
      <hr />
      <h5 class="white-text"> Lucina  </h5>
      <p>
          Lucina es un personaje jugable en Fire Emblem Awakening y una de los tres protagonistas de éste.
          Lucina es la hija de Chrom del futuro y tiene la marca de Naga, conocida como la marca del venerable, en su ojo izquierdo. Ella es una princesa amable y decidida con un gran sentido de justicia, que cree que salvar al mundo es su misión. Ella adora a su padre y siempre se está 
          preocupando por él. Su espada es la Falchion linaje que es la misma espada  que tiene Chrom pero traída del futuro. Dependiendo con quien se case Chrom, Lucina puede ser hermana de Linfan (masculino), Íñigo, Cynthia, Brady o Kjelle, o ser hija única. También es prima de Owain y posiblemente de Linfan (femenino) si Daraen se casa con Lissa o con Emmeryn.
      </p>
      <br />
      <hr />
    <br /><br />

    <div class="container">
        <div class="row">
            <form class="col s12">
                <div class="card white black-text">
                    <div class="card-content">
              <div class="row">
                <div class="input-field col s6">
                  <i class="material-icons prefix">mode_edit</i>
                  <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                  <label for="icon_prefix2">Redacte su pinion sobre mi pagina</label>
                </div>
              </div>
              <!--botones-->
              <button class="button is-success" input type="submit" >
                  <span class="icon is-small">
                      <i class="material-icons prefix">done</i>
                  </span>
                  <span>Save</span>
                </button>

                <button class="button is-success" input type="reset">
                    <span>Delete</span>
                    <span class="icon is-small">
                        <i class="material-icons prefix">delete_forever</i>   
                    </span>
                </button>

            </div>
          </div>
            </form>
          </div>
    </div>   
</div>
      </div>