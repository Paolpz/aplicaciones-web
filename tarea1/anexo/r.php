<?php

    $var_getMenu = isset($_GET['menu']) ? $_GET['menu'] : 'inicio';
    // $var_getMenu = $_GET['menu'];

    switch ($var_getMenu) {
        case "inicio":
            require_once('./paginas/home.php');
            break;

        case "login":
            require_once('./paginas/login.php');
            break;

        case "p":
            require_once('./paginas/p.php');
            break;

        case "pt":
            require_once('./paginas/pt.php');
            break;

        case "comida":
            require_once('./paginas/comida.php');
            break;
            
        case "a":
            require_once('./paginas/a.php');
            break;

        case"alumnos":
        include_once ('./control/alumnos.php');
        $sqlAlumnos = alumnos::consultar();
        include_once './paginas/valumnos.php';
        break;
        
        default:
            require_once('./paginas/home.php');
        }
?>